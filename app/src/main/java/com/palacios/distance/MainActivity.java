package com.palacios.distance;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;


public class MainActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFusedLocationClient;
    boolean isPolyLoaded= false;
    private Polyline polyline;
    public PolylineOptions lineOptions;
    public boolean startDriving =  false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        final Button startStopButton = findViewById(R.id.buttonStartStop);

        startStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!startDriving) {
                    startStopButton.setText("Stop");
                    startDriving = true;
                }else {
                    startDriving = false;
                    startStopButton.setText("Start");
                    mMap.clear();
                    polyline = null;
                    lineOptions = new PolylineOptions();
                    isPolyLoaded = false;
                }
            }
        });


    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        lineOptions = new PolylineOptions();

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mMap.setMyLocationEnabled(true);
            } else {
                checkLocationPermission();
            }
        }

        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());



    }




    protected double calculateDistanceMeters(PolylineOptions points) {
        float totalDistance = 0;

        for(int i = 1; i < points.getPoints().size(); i++) {
            Location currLocation = new Location("this");         //provider is only a name
            currLocation.setLatitude(points.getPoints().get(i).latitude);
            currLocation.setLongitude(points.getPoints().get(i).longitude);

            Location lastLocation = new Location("this");
            lastLocation.setLatitude(points.getPoints().get(i-1).latitude);
            lastLocation.setLongitude(points.getPoints().get(i-1).longitude);

            totalDistance += lastLocation.distanceTo(currLocation);  //distanceTo return in meters


        }
        return (double)Math.round(totalDistance * 10d) / 10d;  //rounded to 1 decimal
    }


    /*------------------------Callback for gps changes-----------------------*/
    LocationCallback mLocationCallback = new LocationCallback() {


        @Override
        public void onLocationResult(LocationResult locationResult) {
            for (Location location : locationResult.getLocations()) {
                if (getApplicationContext() != null) {
                    double lat = (double)Math.round(location.getLatitude() * 10000d) / 10000d;
                    double lng = (double)Math.round(location.getLongitude() * 10000d) / 10000d;
                    ((TextView)findViewById(R.id.TextViewPosition)).setText("Coord "+lat+" "+lng);
                    ((TextView)findViewById(R.id.TextViewAccuracy)).setText("Accuracy:" +String.valueOf(location.getAccuracy()));
                    LatLng position = new LatLng(lat,lng);

                    if(!lineOptions.getPoints().contains(position)){


                        lineOptions.add(position);
                        lineOptions.width(7);
                        lineOptions.color(Color.BLACK);
                        // Drawing polyline in the Google Map
                        if (!isPolyLoaded){
                            polyline = mMap.addPolyline(lineOptions);
                            isPolyLoaded = true;
                        }else{
                            polyline.setPoints(lineOptions.getPoints());

                        }
                        double distanceInKm = (double)Math.round(calculateDistanceMeters(lineOptions)/1000 * 10d) / 10d;  //rounded to 1 decimal
                        ((TextView)findViewById(R.id.TextViewDistance)).setText("distance: "+String.valueOf(distanceInKm) + " Km");
                    }





                }
            }


        }
    };





    public void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                new android.app.AlertDialog.Builder(this)
                        .setTitle("give permission")
                        .setMessage("give permission message")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                            }
                        })
                        .create()
                        .show();
            } else {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        }
    }

}
